var $$ = Dom7;
var debugTag = "mHBSTrainerAppLogs"
// Framework7 App main instance
var app = new Framework7({
  root: '#app', // App root element
  id: 'edu.iupui.soic.biohealth.plhi.mhbs', // App bundle ID - matched to Android package
  name: 'mHBSTraining', // App name
  theme: 'auto',
  init: false,
  // Automatic theme detection
  // App root data
  data: function () {
    return {
      user: {
        username: '',
        pin: '',
        group: ''
      },
      intentReceived: false,
      // secure local storage to hold credentials
      storage: {},
      // video and PDF content
      pdfList: [],
      videoList: [],
      offlineMode: false,
      timeOffline: {
        startTime: Date,
        endTime: Date,
        date: "",
      }
    };
  },
  // App root methods
  methods: {
    triggerOnlineContent: function () {
      console.log("trigger downloading content");
      if (download) {
        app.preloader.show();
        console.log("We can download");
        accessOnlineContent();
        download = false;
      }
      else {
        console.log("Other credentials Read");
        // reset flag since we are done reading
        downloadAble = true;
        //setHeaders();
      }
    },

    // update page visits in db
    increasePageVisits: function (id,page_name) {
      db.transaction(function(tx) {
        tx.executeSql("SELECT * FROM app_usage WHERE id=?", [id], function(tx,res){
          for(var iii = 0; iii < res.rows.length; iii++)
          {
            // get last saved page visits and increase by one
            var page_visits = res.rows.item(iii).page_visits+1;
            // update db
            db.transaction(function(tx) {
              tx.executeSql("UPDATE app_usage SET page_visits = ? WHERE id = ?", [page_visits,id]);
              // updateAppUsageToServer when page visits in local db increases from {pageVisitsThersholdLimit}
              if(page_visits>pageVisitsThersholdLimit)
                updateAppUsageToServer(page_name,id,page_visits,0);
            }, function(err){
              console.error("An error occured while updating page page_visits usage");
            });
          }
        });
      }, function(err){
          console.error("An error occured while getting saved page page_visits usage");
      });

    },

    // update page time spent in db
    increasePageTimeSpent: function (id,time_spent,page_name) {
      db.transaction(function(tx) {
        tx.executeSql("SELECT * FROM app_usage WHERE id=?", [id], function(tx,res){
          // convert time_spent in minutes
          time_spent = time_spent/(1000*60);
          for(var iii = 0; iii < res.rows.length; iii++)
          {
            // get last saved page time spent and increase by this "time_spent"
            var page_time_spent = res.rows.item(iii).page_time_spent+time_spent;
            // update db
            db.transaction(function(tx) {
              tx.executeSql("UPDATE app_usage SET page_time_spent = ? WHERE id = ?", [page_time_spent,id]);
              // updateAppUsageToServer when page_time_spent in local db increases from {pageTimeSpentThersholdLimit} minutes
              if(page_time_spent>pageTimeSpentThersholdLimit)
                updateAppUsageToServer(page_name,id,0,page_time_spent);
            }, function(err){
              console.error("An error occured while updating page page_time_spent usage");
            });
          }
        });
      }, function(err){
          console.error("An error occured while getting saved page page_time_spent usage");
      });

    },

    initialize: function () {
      return this.data
    }
  },
  // App routes
  routes: routes,
});

// initialize app manually, ensures app.data attributes are instantiated from the start
app.init();

// local declarations
var secureParamsStored = 0;
var myPhotoBrowserPopupDark;
var logCount = 0;
var videoCaption = "";
var appServer = 'https://bmgfdev.soic.iupui.edu/api/';
var documentList = [];
var downloadAble = false;
var secureStorageInactive = true;
var currentID;
var appLaunches = 0;
var metaDataLoaded = 0;
var networkUsage = 1;
var paused = 0;
var userId = '';
var pageVisitsThersholdLimit = 10; // when visits of any page exceds this value and user has internet usage got update to dhis2
var pageTimeSpentThersholdLimit = 3; // when time spent in minutes on any page exceds this value and user has internet usage got update to dhis2
var tempCredentials = {
  username: '',
  password: '',
  serverURL: ''
};
var selectedMediaFilter = 1; // 1->all media : 2->pdfs : 3->videos : 4->other files
var completeMediaList = new Array(); //this contains the unfiltered whole content of media that is being shown by applying filters
var pdfMimeTypes = ["application/pdf"];
var videoMimeTypes = ["video/ogg","video/webm","video/.webm","video/.ogv","video/mp4",
"video/.mp4","video/.m4v","video/x-flv","application/x-mpegURL","video/MP2T","video/3gpp",
"video/quicktime","video/x-msvideo","video/x-ms-wmv"];


var download = false;
var storage = window.localStorage;

// Init/Create views
var homeView = app.views.create('#view-home', {
  url: '/'
});

var videoListView = app.views.create('#view-videoList', {
  url: '/mediaPage/'
});

$$('#view-videoList').on('tab:show', function () {
  if(app.data.user.group == 'groupA') {
    app.tab.show("#view-home");
    var sApp = startApp.set({
      "application":"uk.ac.ox.tropicalmedicine.eHBB"
    }).start();
  } else if(app.data.user.group == 'groupC') {
    app.tab.show("#view-home");
  }
});

var guideView = app.views.create('#view-guide', {
  url: '/mhbsmain/'
});

// swiper for images
var swiper = app.swiper.create('.swiper-container', {
  speed: 400,
  spaceBetween: 100
});


function onLoad() {
  // if we don't have credentials in secure storage, send a broadcast, store them, and log the user in
  if (secureStorageInactive) {
    // intent callback received from tracker-capture
    window.plugins.intentShim.onIntent(onIntent);
    // show the preloader while we wait for credentials from tracker-capture
    // set up secure storage, in the callback, broadcast to tracker
    app.data.storage = ss();
  }

  document.addEventListener("deviceready", function(){
    // make a sql database to store data locally
    // fileuri is the local address of the files where it is stored if not downloaded yet it is null
    // window.openDatabase("database-name","version","database description","database size in bytes")
    db = window.openDatabase("mhbsTrainerDb", "1.0", "mhbsTrainer database", 1000000); //will create database tutorialdb or open it
    db.transaction(function(tx) {
        // create media table to store data related to media tab
        tx.executeSql("CREATE TABLE IF NOT EXISTS media (id text primary key, name text, fileuri text,mimeType text)");
        // create app_usage table to store app usage data for analytical purpose
        tx.executeSql("CREATE TABLE IF NOT EXISTS app_usage (id text primary key, page_name text, page_visits integer,page_time_spent integer)");
    }, function(err){
        alert("An error occurred while initializing the app "+err);
    });
    initPageUsageTracker();
  },false);
}

// Local storage setup ------------------

// checkboxes pertaining to the mHBS guide
var checkboxVals = {
  'check1a1': false,
  'check1a2': false,
  'check1a3': false,
  'check1a4': false,
  'check1b1': false,
  'check1b2': false,
  'check1b3': false,
  'check1b4': false,
  'check1c1': false,
  'check1c2': false,
  'check1c3': false,
  'check1c4': false,
  'check1c5': false,
  'check1c6': false,
  'check1c7': false,
  'check2a1': false,
  'check2a2': false,
  'check2a3': false,
  'check2a4': false,
  'check2b1': false,
  'check2b2': false,
  'check2b3': false,
  'check2b4': false,
  'check2c1': false,
  'check2c2': false,
  'check2c3': false,
  'check2c4': false,
  'check2d1': false,
  'check2d2': false,
  'check2d3': false,
  'check2d4': false,
  'check2e1': false,
  'check2e2': false,
  'check2e3': false,
  'check2e4': false,
  'check2e5': false,
  'check2e6': false,
  'check2e7': false,
  'check2e8': false,
  'check3a1': false,
  'check3a2': false,
  'check3a3': false,
  'check3a4': false,
  'check3b1': false,
  'check3b2': false,
  'check3b3': false,
  'check3b4': false,
  'check3c1': false,
  'check3c2': false,
  'check3c3': false,
  'check3c4': false,
  'check3c5': false,
  'check3c6': false,
  'check3c7': false,
  'check3c8': false,
  'check3c9': false,
  'check3c10': false,
  'check3d1': false,
  'check3d2': false,
  'check3d3': false,
  'check3d4': false,
  'check3e1': false,
  'check3e2': false,
  'check3e3': false,
  'check3e4': false,
  'check3f1': false,
  'check3f2': false,
  'check3f3': false,
  'check3f4': false,
  'check3g1': false,
  'check3g2': false,
  'check3g3': false,
  'check3g4': false,
  'check3g5': false,
  'check3g6': false,
  'check3g7': false,
  'check3g8': false,
  'check3g9': false,
  'check3g10': false,
  'check3g11': false,
  'check3g12': false,
  'check4a1': false,
  'check4a2': false,
  'check4a3': false,
  'check4a4': false,
  'check4b1': false,
  'check4b2': false,
  'check4b3': false,
  'check4b4': false,
  'check4c1': false,
  'check4c2': false,
  'check4c3': false,
  'check4c4': false,
  'check4d1': false,
  'check4d2': false,
  'check4d3': false,
  'check4d4': false,
  'check4e1': false,
  'check4e2': false,
  'check4e3': false,
  'check4e4': false,
  'check4e5': false,
  'check4e6': false,
  'check4e7': false,
  'check4e8': false,
  'check4e9': false,
  'check4e10': false,
};

// gets all the pages defined in pages/ and adds to page visits when we first initialize
function setupPageVisits() {
  // "pageVisits" is a pseudo storage item to check if we initialized all the pages
  if (storage.getItem("pageVisits") === null) {
    for (var i in this.app.routes) {
      var pageName;
      var route = this.app.routes[i];
      if (route.url != null) {
        if (route.url.indexOf("pages") !== -1) {
          pageName = route.url.split("/").pop();
          pageName = pageName.substring(0, pageName.indexOf(".html"));
          storage.setItem(pageName, JSON.stringify(0));
        }
      }
    }
    storage.setItem("pageVisits", "true");
  }
}

function initPageUsageTracker() {
  if (storage.getItem("isPageUsageRegisteredInDb") === null) {
    // add rows for every pages in sql-db
    // id = {key in the namespace "trainer-app-usage" on which the data of particular page stores}
    addPageUsageToDB("Wvcu1bXFIvD","homepage");
    addPageUsageToDB("IXx6hyabfgr","about");
    addPageUsageToDB("lewNGzUFDnS","mediaPage");
    addPageUsageToDB("bA8QypQ8EOy","page1");
    addPageUsageToDB("aZ8iXoPhI8J","section1");
    addPageUsageToDB("OGyg1ZBPTyT","section1a");
    addPageUsageToDB("bh2d52QhJDU","section1b");
    addPageUsageToDB("pL8YMHAavzM","section1c");
    addPageUsageToDB("rCBuWSqHRUo","section2");
    addPageUsageToDB("S3a0I7YoKZZ","section2a");
    addPageUsageToDB("eU8z6djmCNo","section2b");
    addPageUsageToDB("aJ4sxqJE39N","section2c");
    addPageUsageToDB("JAMaU6orBTC","section2d");
    addPageUsageToDB("fZFOhojmLck","section2e");
    addPageUsageToDB("DmiccJV9owi","section3");
    addPageUsageToDB("i1uNZG57Vvn","section3a");
    addPageUsageToDB("GHdoj7apebR","section3b");
    addPageUsageToDB("s5ExNdjXBat","section3c");
    addPageUsageToDB("yTHtEpk35ic","section3d");
    addPageUsageToDB("YJjv8U9rLgB","section3e");
    addPageUsageToDB("Zpa8oIgQskR","section3f");
    addPageUsageToDB("nHVwOmfFbK5","section3g");
    addPageUsageToDB("nOCGR7oWXhZ","section4");
    addPageUsageToDB("lronaO33ppS","section4a");
    addPageUsageToDB("D6JG7GBPuIH","section4b");
    addPageUsageToDB("tUuhRqEXJSf","section4c");
    addPageUsageToDB("a1X0TLZ3uk9","section4d");
    addPageUsageToDB("syoRjkPpuLX","section4e");
    addPageUsageToDB("rOSSrVAg3wm","section5");
    addPageUsageToDB("IoJXWMVxkM2","section5a");
    addPageUsageToDB("i9a3kUlUNuY","section5b");
    addPageUsageToDB("iOvJqoJthkR","section6");
    addPageUsageToDB("m9JUI66nrT8","section6a");
    addPageUsageToDB("ZkQkWThW42a","section6b");
    addPageUsageToDB("Jakw1ocIKB9","section6c");
    addPageUsageToDB("ibjg6GDeuyF","section6d");
    addPageUsageToDB("Qiz257H2PPx","section6e");
    addPageUsageToDB("EF9kQZgoXmb","section7");
    addPageUsageToDB("Qp64sTi4jam","mhbsmain");
    addPageUsageToDB("euB2LAVkMNn","privacypolicy");
    addPageUsageToDB("qUVwmmUzZCZ","404");

    storage.setItem("isPageUsageRegisteredInDb", "true");
  }
}

// add page to our local db in app_uage table to track usage
function addPageUsageToDB(id,page_name){
  db.transaction(function(tx) {
    tx.executeSql("INSERT INTO app_usage(id, page_name) VALUES(?,?)", [id, page_name]);
}, function(err){
    console.error("An error occured while creating page tracker in local db");
});
}

// sets the storage item which holds time we have been without internet on app initialize
function setupTimeOffline() {
  if (storage.getItem("timeOffline") === null) {
    storage.setItem("timeOffline", JSON.stringify(0));
  }
}

// set up checkbox values pertaining to mHBS guide on app initialize
function setupCheckBoxValues() {
  if (localStorage.getItem("checkboxVals") === null) {
    for (var checkBoxName in checkboxVals) {
      storage.setItem(checkBoxName, JSON.stringify(checkboxVals[checkBoxName]));
    }
  }
}

// Events ------------------

// event where all three credentials were correctly read, so we can set the download access token
app.on('credentialsRead', function () {
  // we still have tempCredentials, which means we haven't logged in yet
  if (tempCredentials != null) {
    // clear the temp credentials, since we stored them in secure storage
    clearTempCredentials();
    // login
    getPasswordFromSecure(logIn);
  }
  if (downloadAble) {
    app.preloader.hide();
    // todo: optimize
    download = true;
  }
});

// event triggered when network goes online, calculates time between offline and online and sets to storage
app.on('wentOnline', function () {
  var timeElapsed = calculateElapsedTime(app.data.timeOffline.startTime, app.data.timeOffline.endTime);
  var storedOfflineTime = storage.getItem("timeOffline");
  if (storedOfflineTime === null) {
    storage.setItem("timeOffline", timeElapsed);
  }
  else {
    storedOfflineTime = storedOfflineTime + "," + timeElapsed;
    storage.setItem("timeOffline", storedOfflineTime);
  }
  // reset start and end times for next round where we go offline/online
  app.data.timeOffline.startTime = null;
  app.data.timeOffline.endTime = null;
});

// set basic auth request header
function setHeaders() {
  // todo: remove
  app.request.setup({
    headers: {
      'Authorization': 'Basic ' + btoa(tempCredentials.username + ":" + tempCredentials.password)
    }
  });
}

// track writing credentials to secure storage, only continue with three calls, which emits to 'downloadOk'
app.on('storedCredential', function (key) {
  if (key === "username") {
    wroteToSecure();
  } else if (key === "password") {
    wroteToSecure();
  }
  else if (key === "serverURL") {
    wroteToSecure();
  }
});

// track reading credentials from secure storage, only continue with three calls, which emits to 'credentials read'
app.on('gotCredential', function (key, value) {
  if (key === "username") {
    readFromSecure();
    tempCredentials.username = value;
  } else if (key === "password") {
    readFromSecure();
    tempCredentials.password = value;
  }
  else if (key === "serverURL") {
    readFromSecure();
    tempCredentials.serverURL = value;
  }
});


// we are positive credentials were written, so we can get them and login
app.on('wroteCredentials', function () {
  getCredentials();
});

/* triggered when document id, title and content type
   are finished being gathered from server, then
   parse to separate arrays by content type.
*/
app.on('contentType', function () {
  /* make sure we got both thumbnail and duration */
  metaDataLoaded = metaDataLoaded + 1;
  if (metaDataLoaded < 2) {
    return;
  } else {
    metaDataLoaded = 0;
  }
  // hide pre-loader once we downloaded content
  app.preloader.hide();
  for (var i in documentList) {
    if (documentList[i].contentType === "video/webm") {
      app.data.videoList.push(documentList[i]);
    } else if (documentList[i].contentType === "application/pdf") {
      app.data.pdfList.push(documentList[i]);
    }
  }

  // routes user to video list once lists of content are loaded
  homeView.router.navigate('/videoList/');
});

// takes the file name of the path to access if we found the file on device or wrote the file to device
app.on("fileOnDevice", function (filePath) {
  /* this variable must be named photos, if the name is changed, this will not work.
   That is because it is defined in photo browser in framework7
  */
  var photos = [
    {
      html: '<video controls autoplay><source id="myVideo" src="/data/data/edu.iupui.soic.biohealth.plhi.mhbs/files/files' + filePath + '" type=\'video/webm;codecs="vp8, vorbis"\'></video>',
      captions: '',
    }
  ];
  myPhotoBrowserPopupDark = app.photoBrowser.create({
    photos: photos,
    theme: 'dark',
    type: 'popup',
    navbar: true,
    navbarOfText: "/",
    toolbar: false,
  });
  // ready to show video
  app.preloader.hide();
  myPhotoBrowserPopupDark.open();
});

//temporary video for plays
$$(document).on('click', "#videoplay", function () {
  var photos1 = [
    {
	  html: '<iframe src="img/vid/keeping_the_baby_warm.webm" frameborder="0" allowfullscreen></iframe>',
      captions: '',
    }
  ];
  myPhotoBrowserPopupDark1 = app.photoBrowser.create({
    photos: photos1,
    theme: 'dark',
    type: 'popup',
    navbar: true,
    navbarOfText: "/",
    toolbar: false,
  });
  // ready to show video
  myPhotoBrowserPopupDark1.open();
});
// Click Events ------------

/* triggered when we click on a video item in the list in /videoList/
// get the id of the video, check if it exists already, get permission to download
 */
$$(document).on('click', ".pb-standalone-video", function () {
  currentID = this.id;
  videoCaption = this.innerText;
  checkFile();
  getDownloadAccessToken();
});

/* triggered when we click on mhbs tracker in the left panel sidebar of index.html
   ues darryncampbell intent plugin
*/
$$(document).on('click', ".mHBSTracker", function () {
  var sApp = startApp.set({
    "component": ["edu.iupui.soic.bhi.plhi.mhbs.trackercapture", "org.hisp.dhis.android.sdk.ui.activities.SplashActivity"],
    "flags": ["FLAG_ACTIVITY_NEW_TASK"]
  }).start( function() {
      console.log("mHBS Tracker App Started.");
    }, function(error) {
      var sApp = startApp.set({
        "component": ["com.dhis2.debug", "org.dhis2.usescases.splash.SplashActivity"],
        "flags": ["FLAG_ACTIVITY_NEW_TASK"]
      }).start( function() {
          console.log("mHBS Tracker App Started.");
        }, function(error) {
          alert("Install mHBS Tracker Application!!!");
        }
      );
    }
  );
});

/* basically while we are downloading shows the preloader
 */
function getDownloadAccessToken() {
  if (downloadAble) {
    // set this access token to false while we are accessing user information to log them into server
    downloadAble = false;
    getCredentials();
  } else {
    console.log("could not get permission to download content");
  }
}

// checks if file exists on device
function checkFile() {
  var path = '/' + currentID + ".webm";
  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
    // todo: remove
    fileSystem.root.getFile(path, {create: false},
      // callbacks
      fileExists,
      fileDoesNotExist);
  }, getFSFail); //of requestFileSystem

}

/* synchronize write and read to secure storage,
   makes sure if username, password, serverURL set,
   then we can get the credentials/download content
*/
function wroteToSecure() {
  secureParamsStored += 1;
  if (secureParamsStored < 3) {
    return;
  }
  secureParamsStored = 0;
  downloadAble = true;
  // if we wrote three credentials, proceed to download
  app.emit("wroteCredentials");
}

// read values from secure storage
function readFromSecure() {
  secureParamsStored += 1;
  if (secureParamsStored < 3) {
    return;
  }
  secureParamsStored = 0;
  // three credentials read
  app.emit("credentialsRead");
}

// if file exists we can display it
function fileExists(fileEntry) {
  app.emit("fileOnDevice", fileEntry.fullPath);
}

//TODO: need to prevent anything other than binary data writing to file
function fileDoesNotExist() {
  app.preloader.show('blue');
  downloadContent();
}

// write to file fail event
function getFSFail(evt) {
  console.log("ERROR COULD NOT GET FILE" + evt.target.error.code);
}

// get password with downloadBlob callback
function downloadContent() {
  getPasswordFromSecure(downloadBlob);
}

/* download video/pdf content housed on mhbs.info/api/documents/
this function only gets called if the file does not already exist on the device, and after retrieving
password from secure storage
 */
function downloadBlob(password) {
  // holds the id of the video that was clicked
  var id = currentID;
  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
    fs.root.getFile('bot.png', {create: true, exclusive: false}, function (fileEntry) {
      var oReq = new XMLHttpRequest();
      var server = appServer + "documents/" + id + "/data";
      // Make sure you add the domain name to the Content-Security-Policy <meta> element.
      oReq.open("GET", server, true);
      oReq.setRequestHeader('Authorization', 'Basic ' + btoa(app.data.user.username + ":" + password));
      // Define how you want the XHR data to come back
      oReq.responseType = "blob";
      oReq.onload = function (oEvent) {
        var blob = oReq.response; // Note: not oReq.responseText
        if (blob) {
          var reader = new FileReader();
          reader.onloadend = function (evt) {
            // writing the file
            fileToWrite(blob, id);
          };
          reader.readAsDataURL(blob);
        }
        else {
          console.error('we didnt get an XHR response!');
        }
      };
      oReq.send(null);
    }, function (err) {
      console.error('error getting file! ' + err);
    });
  }, function (err) {
    console.error('error getting persistent fs! ' + err);
  });
}

// request file to write to
function fileToWrite(obj, id) {
  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
    fs.root.getFile('/' + id + ".webm", {create: true, exclusive: false}, function (fileEntry) {
      writeFile(fileEntry, obj);
    }, function (fs) {
      console.log("Successfully wrote file" + fs);
    });
  }, function (fileError) {
    console.log("error writing to file" + fileError);
  });
}

// write file
function writeFile(fileEntry, dataObj) {
  // Create a FileWriter object for our FileEntry (log.txt).
  fileEntry.createWriter(function (fileWriter) {
    fileWriter.onwriteend = function () {
      app.emit("fileOnDevice", fileEntry.fullPath);
    };
    fileWriter.onerror = function (e) {
      console.log("Failed file write: " + e.toString());
    };
    // If data object is not passed in,
    // create a new Blob instead.
    if (!dataObj) {
      dataObj = new Blob(['some file data'], {type: 'video/ogg'});
    }
    fileWriter.write(dataObj);
  });
}

// get password from local storage and then get docs from server
function accessOnlineContent() {
  // getPasswordFromSecure(getDocsFromServer);
}

/*
// get list of documents from mhbs.info/api/documents triggered when clicking on 'videos'
function getDocsFromServer(password) {
  var rawDocuments = {
    rawXML: {}
  };
  var server = appServer + "documents.xml";
  // send request
  app.request.get(server, {
      // username: app.data.user.username,
      // password: password
    }, function (data) {
      rawDocuments.rawXML = data;
      // ready to download content
      accessOnlineDocuments(rawDocuments.rawXML);
    },
    function (error) {
      alert(error + "The content is not retrievable");
    })
}
*/

// get XML content from dhis2 web API
function accessOnlineDocuments(rawXML) {
  if (window.DOMParser) {
    var parser = new DOMParser();
    var xmlDoc = parser.parseFromString(rawXML.toString(), "text/xml");
    var documents = xmlDoc.getElementsByTagName("documents")[0].childNodes;
    var tempID;
    /* artificially make it so only one video shows, note: this needs to stay here for the day
    when we need to show more than one video, do not remove, see below as well.
    */
    var semaphoreCount = 0;
    var semaphore = function () {
      // semaphoreCount += 1;
      // if (semaphoreCount < documents.length) {
      //   return;
      // }
      // app.emit('contentType');
    };
    // get a list of ID's and titles
    // swap these lines to change from showing one video to more than one
    for (var i in documents) {
    // for (var i = 0; i < 1; i++) {
      var doc = {
        title: '',
        id: '',
        contentType: '',
        duration: '',
        thumbnail: ''
      };
      tempID = documents[i].id;
      if (tempID != null) {
        doc.id = tempID;
        // grabs video durations, but too time consuming currently
        parseMetaData(doc);
        doc.title = documents[i].textContent;
        getContentTypes(parser, doc, tempID, semaphore);
        documentList.push(doc);
      }
    }
  }
}

// update the app usage to user datastore
function updateAppUsageToServer(page_name,page_id,page_visits,page_time_spent){
  var url = appServer+"userDataStore/trainer-app-usage/"+page_id;
  getFromServer(url,function(request) {
      const xhttp = new XMLHttpRequest();
      var data = JSON.parse(request.response);
      getPasswordFromSecure(function(password) {
        if(data.httpStatus!=null && data.httpStatus=="Not Found"){
          // key with this page name does not created yet
          xhttp.open("POST", url,true);
          xhttp.setRequestHeader('Authorization', 'Basic ' + btoa(app.data.user.username + ":" + password));
          xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
          xhttp.send(JSON.stringify({"page_name":page_name,"page_visits":page_visits,"page_time_spent":page_time_spent}));

          xhttp.onreadystatechange = function() {
            if(xhttp.readyState == 4 && xhttp.status == 201) {
              if(page_visits!=0) resetPageVisitsToDB(page_id);
              if(page_time_spent!=0) resetPageTimeSpentToDB(page_id);
            }
          };
        }else if(data.page_visits!=null){
          var pageVisits = data.page_visits + page_visits;
          var timeSpent = data.page_time_spent + page_time_spent;
          // update usage to datastore
          xhttp.open("PUT", url);
          xhttp.setRequestHeader('Authorization', 'Basic ' + btoa(app.data.user.username + ":" + password));
          xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
          xhttp.send(JSON.stringify({"page_name":page_name,"page_visits":pageVisits,"page_time_spent":timeSpent}));

          xhttp.onreadystatechange = function() {
            if(xhttp.readyState == 4 && xhttp.status == 201) {
              if(page_visits!=0) resetPageVisitsToDB(page_id);
              if(page_time_spent!=0) resetPageTimeSpentToDB(page_id);
            }
          };
        }
      });
  });
}

function resetPageVisitsToDB(id){
  db.transaction(function(tx) {
    tx.executeSql("UPDATE app_usage SET page_visits = ? WHERE id = ?", [0,id]);
  }, function(err){
    console.error("An error occured while reseting page page_visits usage");
  });
}

function resetPageTimeSpentToDB(id){
  db.transaction(function(tx) {
    tx.executeSql("UPDATE app_usage SET page_time_spent = ? WHERE id = ?", [0,id]);
  }, function(err){
    console.error("An error occured while reseting page page_time_spent usage");
  });
}

// change the UI of li of media page based on its synced status
function changeSyncStatus(id,status){
  console.log("changeSyncStatus called "+id+" "+status);
    if(status==1){
      // sync is in progress
      document.getElementById(id+'-progress').style.visibility = "visible";
    }else {
      document.getElementById(id+'-progress').style.visibility = "hidden";
      if(status==0){
        // not synced
        document.getElementById(id+'-sync').src='img/not_synced.png';
      }else if(status==2){
        // synced successfully
        document.getElementById(id+'-sync').src='img/synced.png';
      }else if(status==3){
        // sync error
        document.getElementById(id+'-sync').src='img/sync_error.png';
      }

      // other code is used to stop on going sync progress
    }
}

// change the filter on the basis of selection called up when filter chips called up from media page
function applyFilter(state){
  console.log(state);
  document.getElementById('icon_all_media').style.display = "none"
  document.getElementById('icon_pdf').style.display = "none"
  document.getElementById('icon_videos').style.display = "none"
  document.getElementById('icon_other').style.display = "none"

  if(state==1){
    document.getElementById('icon_all_media').style.display = "unset"
    selectedMediaFilter = 1
  }else if(state==2){
    document.getElementById('icon_pdf').style.display = "unset"
    selectedMediaFilter = 2
  } else if(state==3){
    document.getElementById('icon_videos').style.display = "unset"
    selectedMediaFilter = 3
  } else {
    document.getElementById('icon_other').style.display = "unset"
    selectedMediaFilter = 4
  }
  getFilteredMediaList(showDocuments);
}

// filter the completeMediaList on the basis of current selected filter and pass it to the callBack function
function getFilteredMediaList(callBack){
  if(selectedMediaFilter==1){
    callBack(completeMediaList)
    return;
  }

  var filteredList = new Array();
  for(i = 0; i < completeMediaList.length; i++) {
    if(selectedMediaFilter==2 && pdfMimeTypes.includes(completeMediaList[i].mimeType)){
      // pdf media file
      filteredList.push(completeMediaList[i]);
    }else if(selectedMediaFilter==3 && videoMimeTypes.includes(completeMediaList[i].mimeType)){
      // videos media file
      filteredList.push(completeMediaList[i]);
    }else if(selectedMediaFilter==4 && !pdfMimeTypes.includes(completeMediaList[i].mimeType)
      && !videoMimeTypes.includes(completeMediaList[i].mimeType) && completeMediaList[i].fileuri!=null){
      // other media files
      filteredList.push(completeMediaList[i]);
    }
  }
  callBack(filteredList);
}

// set the list of document to the ul in media tab
function showDocuments(videoListData){
  // sort the list on the basis of name
  videoListData.sort(function (a, b) {
    if (a.displayName > b.displayName) {
        return 1;
    }
    if (b.displayName > a.displayName) {
        return -1;
    }
    return 0;
  });

  ul = document.getElementById('videoListMedia');
  ul.innerHTML=null;
  var i;
  for(i = 0; i < videoListData.length; i++) {

    let li = document.createElement('li');
    li.className = 'item-link';
    li.className += 'item-content';
    li.innerHTML += '<a href="#" class="item-link item-content">'+
    '<div id="'+videoListData[i].id+'-thumbnail" class="item-media"><img src="img/icon1.png" width="44px" height="44px"/></div>'+
    '<div class="item-inner">'+
    '<div class="item-title-row">'+
        '<div class="item-title" font-size=18px>'+videoListData[i].displayName+'</div>'+
        '<div class="loader" id="'+ videoListData[i].id +'-progress" width="10px" height="10px"></div>'+
        '<img id="'+ videoListData[i].id +'-sync" width="20px" height="20px" src="img/not_synced.png" />'+
      '</div>'+
      '<div class="item-subtitle" id="'+ videoListData[i].id +'-duration" font-size=10px;></div>'+
      '<button id="'+ videoListData[i].id +'-button" media-id="'+ videoListData[i].id +'" class="btn" font-size=10px>Download</button>'+
    '</div>'+
    '</a>';
    ul.appendChild(li);

    // hide syncing progress
    document.getElementById(videoListData[i].id+'-progress').style.visibility = "hidden";

    var status = 0; // Not synced
    if(videoListData[i].fileuri!=null && !videoListData[i].fileuri.isEmpty) {
      status=2; // synced successfully
      if(videoMimeTypes.includes(videoListData[i].mimeType)) // document is of a video type
       parseMetaData(videoListData[i]);
      if(pdfMimeTypes.includes(videoListData[i].mimeType)) { // document is of a pdf type
        var myThumbnail = '<img src="img/pdf.png" width="44px" height="44px">';
        document.getElementById(videoListData[i].id+'-thumbnail').innerHTML = myThumbnail;
      }

      document.getElementById(videoListData[i].id+"-button").innerHTML = "Open"

      }
    changeSyncStatus(videoListData[i].id,status)
  }

  var elements = document.getElementsByClassName("btn");

  var handleClickEventOnButton = function() {
      var id = this.getAttribute("media-id");

      document.getElementById(id+"-button").disabled = true;

      // check for the availability of the document if available show it
      checkForMediaAvailability(id,
        function()  {
        /* triggered when media file is not available locally */

        // make the api call to download the document and save it to db
        var url = appServer + 'documents/'+ id + '/data';
        getPasswordFromSecure(function(password) {
          var xhr = new XMLHttpRequest();
          xhr.open('GET', url);
          xhr.onreadystatechange = handler;
          xhr.responseType = 'blob';
          xhr.setRequestHeader('Authorization', 'Basic ' + btoa(app.data.user.username + ":" + password));
          xhr.send();

          // turn on syncing and wait
          changeSyncStatus(id,1);

          function handler() {
            if (this.readyState === this.DONE) {

              if (this.status === 200) {
                if(this.response instanceof Blob){
                  var data_url = URL.createObjectURL(this.response);
                  var contentType = xhr.getResponseHeader("Content-Type");
                  document.getElementById(id+"-button").disabled = false;

                  // This function solely sufficient to download any type of blob and
                  // play by using already existing supported app on the device.
                  downloadFile(id, this.response, this.response.type);
                }else{
                  document.getElementById(id+"-button").disabled = false;
                  // response type not supported, data is not a blob
                  // show syncing error
                  changeSyncStatus(id,3);
                }
              } else {
                document.getElementById(id+"-button").disabled = false;
                console.error(debugTag+' no media :(');
                // ('Unable to Downloaded') - stop syncing progress
                changeSyncStatus(id,4);
              }
            }
          }
        });

        },
        function(doc){
          /* triggered when media file is available locally */
          console.log("opening saved file "+doc.fileuri);
          cordova.plugins.fileOpener2.open(doc.fileuri, doc.mimeType, {
          error: function error(err) {
            document.getElementById(id+"-button").disabled = false;
            console.error(err);
          },
          success: function success() {
            document.getElementById(id+"-button").disabled = false;
            console.log("success with opening the file");
          }
        });
        }
      );
  };

  // add click listner to handel click events to the download/open button
  for (var i = 0; i < elements.length; i++) {
      elements[i].addEventListener('click', handleClickEventOnButton, false);
  }

}

// check whether the file is already downloaded or not
// if it is present we will show it
function checkForMediaAvailability(id,downloadFromServer,openFile){
  // check if the file is already downloaded or its uri is present in db to open
  db.transaction(function(tx) {
    tx.executeSql("SELECT * FROM media WHERE id = ?", [id], function(tx,res){
      var doc = {};
      for(var iii = 0; iii < res.rows.length; iii++)
      {
        doc['id'] = res.rows.item(iii).id;
        doc['displayName'] = res.rows.item(iii).name;
        doc['fileuri'] = res.rows.item(iii).fileuri;
        doc['mimeType'] = res.rows.item(iii).mimeType;
        break;
      }

      if(doc.fileuri!=null && !doc.fileuri.isEmpty){
        openFile(doc);
      }else{
        downloadFromServer();
      }
    });
  }, function(err){
      // An error occured
      changeSyncStatus(id,3);
  });
}

// add id & name of the new document to our local db
function addNewDocumentToDB(id,name){
  db.transaction(function(tx) {
    tx.executeSql("INSERT INTO media(id, name) VALUES(?,?)", [id, name]);
}, function(err){
    console.error("An error occured while saving the document");
});
}

// update name of the document in db
function updateDocumentNameInDb(id,name){
  db.transaction(function(tx) {
    tx.executeSql("UPDATE media SET name = ? WHERE id = ?", [name,id]);
}, function(err){
    console.error("An error occured while saving the document");
});
}

// update fileuri and mimeType of the document in db
function updateMediaFileLocationInDb(id,fileuri,mimeType){
  db.transaction(function(tx) {
    tx.executeSql("UPDATE media SET fileuri = ?, mimeType = ? WHERE id = ?", [fileuri,mimeType,id]);
}, function(err){
    console.error("An error occured while updating the documents fileuri");
});
}

// extract a files from blob -> save it to the db -> open it in the supported app
function downloadFile(id, blob, mimeType) {
  // we will be using the id as a file name to save its data uniquely
  if (window.cordova && cordova.platformId !== "browser") {
    document.addEventListener("deviceready", function () {

      window.requestFileSystem(
        window.PERSISTENT,
        0,
        function (fs) {
          fs.root.getFile(
            id,
            {
              create: true,
              exclusive: false
            },
            function (file) {
              file.createWriter(
                function (fileWriter) {
                  fileWriter.write(blob);

                  fileWriter.onwriteend = function () {
                    var url = file.toURL();

                    // update file location to DB
                    updateMediaFileLocationInDb(id,url,mimeType);

                    // show sync success ui
                    changeSyncStatus(id,2);

                    // update in completeMediaList
                    document.getElementById(id+"-button").innerHTML = "Open";
                    for(var i=0;i<completeMediaList.length;i++)
                      if(completeMediaList[i].id==id){
                        completeMediaList[i].fileuri = url;
                        completeMediaList[i].mimeType = mimeType;
                      }
                  };

                  fileWriter.onerror = function (err) {
                    console.error(err);
                    // stop syncing progress
                    changeSyncStatus(id,4);
                  };
                },
                function (err) {
                  console.error(err);
                  // stop syncing progress
                  changeSyncStatus(id,4);
                }
              );
            },
            function (err) {
              console.error(err);
              // stop syncing progress
              changeSyncStatus(id,4);
            }
          );
        },
        function (err) {
          console.error(err);
          // stop syncing progress
          changeSyncStatus(id,4);
        }
      );
    });
  } else {
    saveAs(blob, id);
    // stop syncing progress
    changeSyncStatus(id,4);
  }
}

// not used for now it can play a video in PhotoBrowserPopupDark
function playVideo(html_data){
  var photos1 = [
    {
      html : html_data,
      captions: ''
    }
  ];
  myPhotoBrowserPopupDark1 = app.photoBrowser.create({
    photos: photos1,
    theme: 'dark',
    type: 'popup',
    navbar: true,
    navbarOfText: "/",
    toolbar: false,
  });
  // ready to show video
  myPhotoBrowserPopupDark1.open();
}

// load list of documents to media tab
function loadListofDocuments(){
  // Do both works means instantly show documents available in DB and call the api to update there status

  // load locally store documents
  loadMediaListFromDB();

  // call api to update document status
  downloadDocumentsFromServer();

  // set ui as per last applied filter
  applyFilter(selectedMediaFilter);
}

// load documents from server via api call and update DB
function downloadDocumentsFromServer(){
  console.log("downloadDocumentsFromServer");
  var url = appServer + 'documents';
  app.preloader.show;
  getFromServer(url,function(request){
    if(request.status == 200){
      var myArr = JSON.parse(request.response);
      var mediaList = myArr.documents;

      /*
        mediaList is the updated list of documents available on dhis2
        Now it's need to update our local sql db to update it with new media list
      */
      completeMediaList = mediaList;
      updateNewMediaListToDB(mediaList);
    }else{
      console.log("error in downloading updated documents list from server ")
    }
  });
}

//load list of media documents stored locally
function loadMediaListFromDB(){
  db.transaction(function(tx) {
    tx.executeSql("SELECT * FROM media", [], function(tx,res){
      var documents = new Array();
      for(var iii = 0; iii < res.rows.length; iii++)
      {
        var doc = {};
        doc['id'] = res.rows.item(iii).id;
        doc['displayName'] = res.rows.item(iii).name;
        doc['fileuri'] = res.rows.item(iii).fileuri;
        doc['mimeType'] = res.rows.item(iii).mimeType;
        documents.push(doc);
      }
      completeMediaList = documents;
      getFilteredMediaList(showDocuments);
    });
  }, function(err){
      alert("An error occured while displaying saved documents");
  });
}

function updateNewMediaListToDB(mediaList){
  /*
    There are three types of documents
    - Update with name or content in dhis2 ( we need to update there record in db )
    - Deleted from dhis2 - They are present in medialist but not on our local list ( need to delete them entirely from db )
    - Added newly to dhis2 - They are not present in our local list ( need to add them to db )
  */

  var queryDelete = "DELETE FROM media WHERE id NOT IN (";
  for(i = 0; i < mediaList.length; i++) {
    queryDelete+=("'"+mediaList[i].id+"'");
    if(i<mediaList.length-1) queryDelete+=",";
    else queryDelete+=")";
  }

  // delete the documents that are deleted from dhis2
  db.transaction(function(tx) {
    tx.executeSql(queryDelete, [], function(){
      // get the new list of documents
      db.transaction(function(tx1) {
        tx1.executeSql("SELECT * FROM media", [], function(tx1,res){
         var updatedList = new Array();
          var documentsNeedUpdate = new Map();
          for(var iii = 0; iii < res.rows.length; iii++)
            documentsNeedUpdate.set(res.rows.item(iii).id,new Array(res.rows.item(iii).fileuri,res.rows.item(iii).mimeType));

          for(var j=0;j<mediaList.length;j++){
            // make a doc to push in updated list
            var doc = {};
            doc['id'] = mediaList[j].id;
            doc['displayName'] = mediaList[j].displayName;

            if(documentsNeedUpdate.has(mediaList[j].id)){
              var documentDetails = documentsNeedUpdate.get(mediaList[j].id);

              // use the previous file location as fileuri
              doc['fileuri'] = documentDetails[0];
              doc['mimeType'] = documentDetails[1];
              // this document need to be updated
              updateDocumentNameInDb(mediaList[j].id,mediaList[j].displayName);
            }else{
              // no file uri saved
              doc['fileuri'] = null;
              doc['mimeType'] = null;
              // this is newly added document
              addNewDocumentToDB(mediaList[j].id,mediaList[j].displayName);
            }

            // push doc to list
            updatedList.push(doc);
          }

          completeMediaList = updatedList;
          // update UI
          getFilteredMediaList(showDocuments);

        });
      }, function(err){
           // "An error occured while loading saved documents"
           console.log("error in getting documents that needs update");
      });
    });
  }, function(err){
      // An error occured while deleting the saved documents
      console.log("error in deleting documents : "+err);
  });

}

// gets video duration, can also grab other desired data here
function parseMetaData(doc) {
  var video = document.createElement("video");
  // preload a video file uri
  video.src = doc.fileuri;
  video.preload = 'metadata';
  // once meta data is loaded can be grabbed, but not before then
  video.addEventListener("loadedmetadata", function () {
    video.currentTime = 5;
    var minutes = Math.floor(video.duration / 60);
    var seconds = (video.duration % 60).toFixed(0);
    if (seconds.toString().length === 1) {
      seconds = seconds.toString().concat("0");
    }
    doc.duration = minutes + ":" + seconds;
    document.getElementById(doc.id+'-duration').innerHTML = "Length - "+minutes + ":" + seconds+" minutes";
  });

  video.src = doc.fileuri+"#t=0.2";
  video.addEventListener('loadeddata', function () {
    // specify as lazy load so we only proceed when image is ready.
    var myThumbnail = '<img width="44px" height="44px" src="' + thumbnail(video) + '">';
    doc.thumbnail = myThumbnail;
    document.getElementById(doc.id+'-thumbnail').innerHTML = myThumbnail;
  }, false);
}

function thumbnail(video) {
  var canvas = document.createElement('canvas');
  canvas.getContext('2d').drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
  var img = document.createElement("img");
  img.src = canvas.toDataURL('image/jpeg');
  return img.src
}

// gets type of document, video/pdf/ etc
function getContentTypes(parser, doc, id, callback) {
  var server = appServer + "documents/" + id + ".xml";
  // send request
  app.request.get(server, {}, function (data) {
      var xmlDoc = parser.parseFromString(data, "text/xml");
      var nodeList = xmlDoc.childNodes[0];
      var node = nodeList.childNodes;

      for (var key of node.values()) {
        if (key.nodeName === "contentType") {
          doc.contentType = key.childNodes[0].data;
        }
      }
      callback();
    },
    function (error) {
      alert(error + "The content is not retrievable");
    });
}

// add event listeners
document.addEventListener("deviceready", function (e) {
  document.addEventListener("offline", wentOffline, false);
  document.addEventListener("online", wentOnline, false);
  document.addEventListener("pause", onPause, false);
  document.addEventListener("resume", onResume, false);
  document.addEventListener("backbutton", function (e) {
    homeView.router.back();
  }, false);
});

// event callbacks -----------
var onPause = function () {
  paused++;
};

var onResume = function () {
  console.log("app Resumed");

  appLaunches = appLaunches + 1;
  // store app launches on resume, how many times we launched gets sent to server
  storage.setItem("appLaunches", JSON.stringify(appLaunches));

  // always post when app launches if the app pin is set
  if (app.data.user.pin !== '') {
    setupTimeOffline();
    //trackNumLoginsByPin();
  }
};

// for when network is offline
wentOffline = function (e) {
  // if we started the app and have never been online, networkUsage = 0,
  // otherwise it starts at 1.
  if ((parseInt(storage.getItem("appLaunches")) === 0)) {
    networkUsage = 0;
  }
  //masked below lines app.preloader and alert temporarly
  //app.preloader.show('blue');
  app.data.timeOffline.startTime = new Date();
  //alert("Please connect to the internet to use the mHBS training app");
  app.data.offlineMode = true;
};

// for when network is online
wentOnline = function (e) {
  networkUsage++;
  app.preloader.hide();
  app.data.timeOffline.endTime = new Date();
  app.data.offlineMode = false;
  //trigger
  app.emit("wentOnline");
};

// calculates elapsed time in minutes
function calculateElapsedTime(startTime, endTime) {
  if (startTime <= endTime) {
    var seconds = Math.round((endTime - startTime) / 1000);
    if (seconds <= 60) {
      return seconds + "s";
    } else {
      var minutes = Math.round(seconds / 60);
      return minutes;
    }
  } else {
    return 0;
  }
}

/* send broadcast to tracker capture, uses darryncampbell plugin */
function sendBroadcastToTracker() {
  window.plugins.intentShim.sendBroadcast({
      action: 'edu.iupui.soic.bhi.plhi.mhbs.training.activities.SharedLoginActivity'
    },
    function () {
      console.log("sent Broadcast");
    },
    function () {
      console.log(" failed to send broadcast");
      alert('Please install and launch this app through mHBS tracker-capture')
    }
  );
}

// secure storage plugin requires android users to have pin lock on device
var securityFunction = function () {
  navigator.notification.alert(
    'Please enable the screen lock on your device. This app cannot operate securely without it.',
    function () {
      app.storage.secureDevice(
        function () {
          _init();
        },
        function () {
          _init();
        }
      );
    },
    'Screen lock is disabled'
  );
};

// create secure storage, set as app.data.storage
var ss = function () {
  return new cordova.plugins.SecureStorage(
    function () {
      // we have storage so broadcast for login info
      sendBroadcastToTracker();
    },
    securityFunction,
    'mHBS_Hybridapp');
};

// get password from secure storage and login
function logIn() {
  getPasswordFromSecure(loginOk);
}

// we got password, we can login
function loginOk(password) {
  var server = appServer + "26/me/";
  // send request
  app.request.get(server, {
      username: app.data.user.username,
      password: password
    }, function (data) {
      if (data.indexOf(app.data.user.username) === -1) {
        credentialsFailAlert();
      } else {
        secureStorageInactive = false;
      }
    },
    function (error) {
      // if we have internet and reached here display error
      if (!app.data.offlineMode) {
        credentialsFailAlert();
      }
    });
}

// something went wrong, if we are offline, it will display a different message
function credentialsFailAlert() {
  alert('Login was not successful, please login mHBS tracker-capture ');
}

// clear tempCredentials since they are stored in secure storage, which is more secure
function clearTempCredentials() {
  tempCredentials.username = null;
  tempCredentials.password = null;
  tempCredentials.serverURL = '';
}

// set user name for our app when we stored credentials upon login
function setAppUsername() {
  app.data.storage.get(function (value) {
    app.data.user.username = value;
  }, function (error) {
    console.log(error);
  }, 'username');
}

// tracks how many times each person / pin logged in
function trackNumLoginsByPin() {
  var numLogins = storage.getItem(app.data.user.pin);
  if (isNaN(parseInt(numLogins)) || parseInt(numLogins) === 0) {
    numLogins = 1;
  } else {
    numLogins = parseInt(numLogins) + 1;
  }
  storage.setItem(app.data.user.pin, JSON.stringify(numLogins));
  setupTimeOffline();
  postEventData();
}

/* handle any incoming intent, uses darryncampbell intent plugin */
function onIntent(intent) {
  var credentialsArr = parseCredentials(intent);
  // if the intent had data, need to log in
  if (credentialsArr != null) {
    if (credentialsArr.length === 6) {
      tempCredentials.username = credentialsArr[0];
      tempCredentials.password = credentialsArr[1];
      tempCredentials.serverURL = credentialsArr[2];
      app.data.user.pin = credentialsArr[3];
      app.data.user.orgUnit = credentialsArr[4];
      app.data.user.trackerIds = credentialsArr[5];
      // set app headers
      setHeaders();
      if (!isEmpty(tempCredentials.username) && !isEmpty(tempCredentials.password) && !isEmpty(tempCredentials.serverURL)) {
        // storeCredentials
        storeCredentials();
        // login
      }
    } else {
      loginAlert();
    }
  }
}

// we got an intent with credentials but it did not contain all the credentials (most likely)
function loginAlert() {
  alert("Please login tracker-capture");
}

// store tempCredentials received from tracker-capture to local storage
function storeCredentials() {
  app.data.storage.set(function () {
    // set username for our app
    setAppUsername();
    app.emit('storedCredential', "username");
  }, function (error) {
    console.log("storedCredential" + error);
  }, 'username', tempCredentials.username);

  app.data.storage.set(function () {
    app.emit('storedCredential', "password");
  }, function (error) {
    console.log("storedCredential" + error);
  }, 'password', tempCredentials.password);

  app.data.storage.set(function () {
    app.emit('storedCredential', "serverURL");
  }, function (error) {
    console.log("storedCredential Error" + error);
  }, 'serverURL', tempCredentials.serverURL);
}

// get credentials from storage, and makes sure all 3 are validly set using gotCredential event tokens
function getCredentials() {
  app.data.storage.get(function (value) {
    app.emit('gotCredential', "username", value);
  }, function (error) {
    console.log("username" + error);
  }, 'username');

  app.data.storage.get(function (value) {
    app.emit('gotCredential', "password", value);
  }, function (error) {
    console.log("password" + error);
  }, 'password');

  app.data.storage.get(function (value) {
    app.emit('gotCredential', "serverURL", value);
  }, function (error) {
    console.log("serverURL" + error);
  }, 'serverURL');
}

// get the credentials from the JSON via tracker-capture
function parseCredentials(intent) {
  if (intent != null) {
    if (intent.extras != null && intent.extras['key:loginRequest']!=null) {
      app.data.intentReceived = true;
      return intent.extras['key:loginRequest'];
    }else{
      loginAlert();
    }
  } else {
    loginAlert();
  }
}

// Helpers ----------

function isEmpty(str) {
  return (!str || 0 === str.length);
}

function getDateStamp() {
  var currentDate = new Date();
  return currentDate.getDate() + "/" + (currentDate.getMonth() + 1) + "/" + currentDate.getFullYear();
}

function getYesterdayDate(){
  var currentDate = new Date();
  return currentDate.getFullYear()+"-"+(currentDate.getMonth() + 1)+"-"+(currentDate.getDate()-1);
}

function getTodayDate(){
  var currentDate = new Date();
  return currentDate.getFullYear()+"-"+(currentDate.getMonth() + 1)+"-"+(currentDate.getDate());
}

function getDateTimeStamp() {
  var currentDate = new Date();
  return getDateStamp() + "  " + getTimeStamp();
}

function getTimeStamp() {
  var currentDate = new Date();
  return currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();
}

function convertSecondsToMinutes(seconds) {
  return Math.round(seconds / 60);
}

// gets password from secure, pass any function as a success callback
var getPasswordFromSecure = function (callback) {
  app.data.storage.get(
    function (value) {
      callback(value);
    },
    function (error) {
      console.log('Error' + error);
    },
    'password');
};

// Metric capture ------------------
function getNumberOfScreens() {
  var numberOfScreens = 0;
  for (var i in this.app.routes) {
    var pageName;
    var route = this.app.routes[i];
    if (route.url != null) {
      if (route.url.indexOf("pages") !== -1) {
        pageName = route.url.split("/").pop();
        pageName = pageName.substring(0, pageName.indexOf(".html"));
        if (storage.getItem(pageName) != 0) {
          numberOfScreens++;
        }
      }
    }
  }
  return numberOfScreens;
}

// combine stored seconds and minutes offline
function getStoredTimeOffline() {
  var elapsedTimes = storage.getItem('timeOffline');
  var elapsedTimeArr = elapsedTimes.split(",");
  var minutes = 0;
  var seconds = 0;

  for (var t in elapsedTimeArr) {
    if (elapsedTimeArr[t].indexOf("s") !== -1) {
      // accumulate seconds
      seconds += parseInt(elapsedTimeArr[t].substring(0, elapsedTimeArr[t].length - 1));
    }
    else {
      // accumulate minutes
      if (!isNaN(parseInt(elapsedTimeArr[t]))) {
        minutes += parseInt(elapsedTimeArr[t]);
      }
    }
  }

// if the seconds make up more than a minute, add it to minutes offline
  if (seconds > 60) {
    minutes += convertSecondsToMinutes(seconds);
  }
  return minutes;
}

// log page visits
function logPageVisit(pageName) {
  var numberOfPageVisits = localStorage.getItem(pageName);
  numberOfPageVisits = parseInt(numberOfPageVisits) + 1;
  storage.setItem(pageName, numberOfPageVisits);
}

// used to initialize checkboxes to false
function initCheckboxesToFalse() {
  $$('input[type="checkbox"]').prop('checked', false);
}

// initializes checkboxes to stored values
function initCheckboxesToStoredVal(checkBoxes) {
  for (var i = 0; i < checkBoxes.length; i++) {
    var checkBoxVal = storage.getItem(checkBoxes[i].id);
    if (checkBoxVal.toString() === "true") {
      document.getElementById(checkBoxes[i].id).checked = true;
    } else {
      document.getElementById(checkBoxes[i].id).checked = false;
    }
  }
}

// set up checkbox listeners for whole app
function setUpCheckBoxListeners() {
  for (var key in checkboxVals) {
    var checkboxDomID = "#" + key;
    $$(document).on('change', checkboxDomID, function () {
      var thisID = this.id;
      var checkboxVal = storage.getItem(thisID);
      if (this.checked) {
        checkboxVal = true;
      } else {
        checkboxVal = false;
      }
      storage.setItem(thisID, JSON.stringify(checkboxVal));
    });
  }
}

// set up page events for all pages in app
function setUpPageEvents() {
  for (var i in this.app.routes) {
    var pageName;
    var route = this.app.routes[i];
    if (route.url != null) {
      if (route.url.indexOf("pages") !== -1) {
        pageName = route.url.split("/").pop();
        pageName = pageName.substring(0, pageName.indexOf(".html"));
        setUpPageBeforeInEvent(pageName);
        setUpPageBeforeOutEvent(pageName);
        setUpAfterOutEvent(pageName);
      }
    }
  }
}

// set up page before in events
function setUpPageBeforeInEvent(pageName) {
  $$(document).on('page:beforein', '.page[data-name="' + pageName + '"]', function (e, page) {
    logPageVisit(page.name);
    // if the page has checkboxes, init checkboxes to stored value
    var checkBoxes = $$('input[type="checkbox"]');
    if (checkBoxes.length > 0) {
      initCheckboxesToStoredVal(checkBoxes);
    }
  });
}

// set up page before out events
function setUpPageBeforeOutEvent(pageName) {
  $$(document).on('page:beforeout', '.page[data-name="' + pageName + '"]', function (e, page) {
  });
}

// set up page after out events
function setUpAfterOutEvent(pageName) {
  $$(document).on('page:afterout', '.page[data-name="' + pageName + '"]', function (e, page) {
  });
}

function setOrgUnit(userId){
  var index = app.data.user.pin.split(',').indexOf(userId);
  return app.data.user.orgUnit.split(',')[index];
}

function setTrackerIds(userId){
  var index = app.data.user.pin.split(',').indexOf(userId);
  return app.data.user.trackerIds.split(',')[index];
}

function postEventData() {
  registerInAppUsage();
  eventPayload['eventDate'] = getTodayDate();
  eventPayload['orgUnit'] = setOrgUnit(userId);
  eventPayload['trackedEntityInstance'] = setTrackerIds(userId);
  eventPayload['storedBy'] = app.data.user.username;
  console.log("sending Payload: " + JSON.stringify(eventPayload));
  for (var i in eventPayload['dataValues']) {
    // todo: check this val
    // Number of abrupt exits or incomplete workflow for mHBS training app
    if (eventPayload['dataValues'][i].dataElement === 'ZYQJ87n45ye') {
      eventPayload['dataValues'][i].value = paused.toString();
    }
    // send time offline in minutes
    else if (eventPayload['dataValues'][i].dataElement === 'qOyP28eirAx') {
      eventPayload['dataValues'][i].value = getStoredTimeOffline().toString();
    }
    // send logins by pin
    else if (eventPayload['dataValues'][i].dataElement === 'getqONgfDtE') {
      eventPayload['dataValues'][i].value = storage.getItem(app.data.user.pin).toString();
    }
    // get number of screens
    else if (eventPayload['dataValues'][i].dataElement === 'RrIe9CA11n6') {
      eventPayload['dataValues'][i].value = getNumberOfScreens().toString();
    }
    // number of times app was started
    else if (eventPayload['dataValues'][i].dataElement === 'BgzISR1GmP8') {
      eventPayload['dataValues'][i].value = storage.getItem("appLaunches").toString();
    }
    // number of times there was network usage
    else if (eventPayload['dataValues'][i].dataElement === 'qbT1F1k8cD7') {
      eventPayload['dataValues'][i].value = networkUsage.toString();
    }
    //console.log("EVENT PAY: " + eventPayload['dataValues'][i].dataElement + " " + eventPayload['dataValues'][i].value);
    //console.log("-----------------");

  }
  postPayload();
  clearPayloadValues();
}

// get password and then post payload
function postPayload() {
  getPasswordFromSecure(makeEventPostRequest);
}

// Event Payload with params relating to mHBS training app posted to the program events on DHIS2
var eventPayload = {
  "program": "dbEHqQVQV5j",
  "orgUnit": "",
  "trackedEntityInstance": "",
  "eventDate": "",
  "programStage":"TLukEU2tvvB",
  "status": "COMPLETED",
  //"trackedEntityInstance": "vmhlccEpW4Q",
  "storedBy": "",
  "coordinate": {
    "latitude": 59.8,
    "longitude": 10.9
  },
  "dataValues": [
    // Number of abrupt exits or incomplete workflow for mHBS training app
    {"dataElement": "ZYQJ87n45ye", "value": ""},
    // Number of mHBS training app logins by pin
    {"dataElement": "getqONgfDtE", "value": ""},
    // Number of minutes mHBS training app was used offline
    {"dataElement": "qOyP28eirAx", "value": ""},
    // Number of screens used in mHBS training app
    {"dataElement": "RrIe9CA11n6", "value": ""},
    // Number of times mHBS training app was started
    {"dataElement": "BgzISR1GmP8", "value": ""},
    // Number of times mHBS training app was with network usage
    {"dataElement": "qbT1F1k8cD7", "value": ""},
  ]
};

//this function registers trackedEntityInstance in app usage
function registerInAppUsage(){
  var registerpayload = {
    "program": "dbEHqQVQV5j",
    "orgUnit": setOrgUnit(userId),
    "trackedEntityInstance": setTrackerIds(userId),
    "enrollmentDate": getYesterdayDate(),
    "incidentDate": getYesterdayDate()
  }
  var eventServer = appServer + "27/enrollments";
  app.request({
    url: eventServer,
    dataType: 'json',
    processData: false,
    crossDomain: true,
    data: JSON.stringify(registerpayload),
    method: 'POST',
    contentType: 'application/json',
    beforeSend: function () {
      //do anything before sending payload
    },
    success: function (data, status, xhr) {
      console.log("Success" + data);
      //Post request completed
    },
    error: function (xhr, status) {
      console.log("Failure: " + JSON.stringify(xhr));
    }
  });
}

function makeEventPostRequest(password) {
  var eventServer = appServer + "27/events";
  app.request({
    url: eventServer,
    dataType: 'json',
    processData: false,
    crossDomain: true,
    data: JSON.stringify(eventPayload),
    method: 'POST',
    contentType: 'application/json',
    beforeSend: function () {
      //function that triggers before running
    },
    success: function (data, status, xhr) {
      console.log("Success" + data);
      //Post request completed
    },
    error: function (xhr, status) {
      console.log("Failure: " + JSON.stringify(xhr));
    }
  });
}

// can use to reset values after we send payload
function clearPayloadValues() {
  networkUsage = 1;
  storage.setItem("appLaunches", JSON.stringify(0));
  setupPageVisits();
  storage.setItem(app.data.user.pin, JSON.stringify(0));
  storage.setItem(app.data.user.orgUnit, JSON.stringify(0));
  storage.setItem(app.data.user.trackerIds,JSON.stringify(0));
  storage.setItem("timeOffline", null);
}

function getFromServer(url, cFunction) {
  getPasswordFromSecure(function(password) {
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function() {cFunction(this);}
    xhttp.open("GET", url);
    xhttp.setRequestHeader('Authorization', 'Basic ' + btoa(app.data.user.username + ":" + password));
    xhttp.send();
  });
}
